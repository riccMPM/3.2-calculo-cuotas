package world.hello.com.pe.calculocuotaas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CuotaActivity extends AppCompatActivity {

    TextView tviMonto;
    RecyclerView rviListado;
    CuotaAdapter adapter;

    int meses, monto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuota);

        tviMonto = findViewById(R.id.tviMonto);
        rviListado = findViewById(R.id.rviListado);

        //Obtenemos valores del activity Main
        Intent intent = getIntent();
        monto = intent.getIntExtra("monto", 0);
        meses = intent.getIntExtra("meses", 0);

        llenarListado();


    }

    private void llenarListado() {
        List<Integer> lstMeses = new ArrayList<>();
        for (int i = 0; i < meses; i++) {
            lstMeses.add(i);
        }

        adapter = new CuotaAdapter(lstMeses, monto, meses);


        //Establecemos el LinearLayoutManager
        rviListado.setLayoutManager(new LinearLayoutManager(this));
        rviListado.setAdapter(adapter);
    }
}

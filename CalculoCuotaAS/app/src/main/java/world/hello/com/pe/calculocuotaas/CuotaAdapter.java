package world.hello.com.pe.calculocuotaas;

import android.support.v7.widget.RecyclerView;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


/**
 * Created by Riccardomp on 17/03/18.
 */

public class CuotaAdapter extends RecyclerView.Adapter<CuotaAdapter.ViewHolder> {


    List<Integer> lstCuotas;
    int totalMonto;
    int meses;


    public CuotaAdapter(List<Integer> lstCuota, int totalMonto, int meses) {
        this.lstCuotas = lstCuota;
        this.totalMonto = totalMonto;
        this.meses = meses;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Este metodo hace la relacion con el layout del item
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cuota, parent, false);


        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Este metodo itera de acuerdo a lo que indicas en el metodo getItemCount

        int valor = lstCuotas.get(position);
        int montoMes = totalMonto / meses;

        holder.tviCuota.setText("Numero Cuota: " + position + " . Monto a pagar: " + montoMes);


    }

    @Override
    public int getItemCount() {
        //Aqui indicas cuantas filas tendra tu listado
        return lstCuotas.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tviCuota;
        ImageView iviMoney;

        public ViewHolder(View v) {
            super(v);

            tviCuota = v.findViewById(R.id.tviCuota);
            iviMoney = v.findViewById(R.id.iviMoney);
        }
    }

}

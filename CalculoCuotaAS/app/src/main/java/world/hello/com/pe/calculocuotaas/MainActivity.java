package world.hello.com.pe.calculocuotaas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText eteMonto;
    Spinner spiMeses;
    Button butNavegar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eteMonto = findViewById(R.id.eteMonto);
        spiMeses = findViewById(R.id.spiMeses);
        butNavegar = findViewById(R.id.butNavegar);

        butNavegar.setOnClickListener(this);

        llenarSpinner();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, CuotaActivity.class);

        //Obtenemos el valor del Monto
        int valorMonto = Integer.parseInt(eteMonto.getText().toString());
        intent.putExtra("monto", valorMonto);

        //Obtenemos valor del spinner
        String valorSpinner = spiMeses.getSelectedItem().toString();
        intent.putExtra("meses", Integer.parseInt(valorSpinner));

        //Vamos al activity Cuota
        startActivity(intent);

    }


    private void llenarSpinner() {
        // Spinner Drop down elements
        List<Integer> lstMeses = new ArrayList<>();

        for (int i = 1; i <= 12; i++) {
            lstMeses.add(i);
        }

        // Creating adapter for spinner
        ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, lstMeses);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spiMeses.setAdapter(dataAdapter);

    }
}
